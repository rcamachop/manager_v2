﻿using Microsoft.Owin.Security.OAuth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Cors;
//using Bussiness;
using Bussiness;
using Dto;

namespace Backend_Manager.Class
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class OAuth : OAuthAuthorizationServerProvider
    {
        public ApiBussiness Bussines = new ApiBussiness(Properties.Settings.Default.Conexion);
        // public AES AES = new AES();

        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();
        }
        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            try
            {
               // var 
                var Autenticacion = Bussines.GetLoginToken(context.UserName, context.Password);

                var Valid = false;
                if (Autenticacion.Count > 0)
                {
                    Valid = true;
                }

                if (Valid)
                {
                    var identity = new ClaimsIdentity(context.Options.AuthenticationType);
                    identity.AddClaim(new Claim("username", context.UserName));
                    identity.AddClaim(new Claim("password", context.Password));
                    context.Validated(identity);
                }
                else
                {
                    context.SetError("invalid_grant", "Usuario y/o contraseña incorrectos." + Autenticacion);
                    return;
                }

            }
            catch (Exception ex)
            {
                context.SetError("invalid_grant", "Usuario y/o contraseña incorrectos.");
                return;
            }

        }
    }
}