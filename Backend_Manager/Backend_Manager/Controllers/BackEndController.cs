﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Bussiness;
using Dto;

namespace Backend_Manager.Controllers
{
    public class BackEndController : ApiController
    {
        public ApiBussiness Bussines = new ApiBussiness(Properties.Settings.Default.Conexion);

        [Authorize]
        [HttpPost, Route("GetLogin")]
        public List<LoginDto> GetLogin([FromBody] TblUsuarios tblUsuarios)
        {
            var respuesta= Bussines.GetLogin(tblUsuarios.valorUsuario, tblUsuarios.clave);
            return respuesta;
        }
    }
}
