﻿using Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Dal
{
    public class ApiDal : Dapper
    {
        public ApiDal(string ConexionString)
        {
            Conexion = ConexionString;
        }


        public List<LoginDto> GetLogin(string Usuario, string Password)
        {
            return ListQuery<object, LoginDto>("dbo.SP_GetLogin", new { Usuario = Usuario, Password = Password });
        }
        public List<LoginDto> GetLoginToken(string Usuario, string Password)
        {
            return ListQuery<object, LoginDto>("dbo.SP_GetLoginToken", new { Usuario = Usuario, Password = Password });
        }
        
    }
}
