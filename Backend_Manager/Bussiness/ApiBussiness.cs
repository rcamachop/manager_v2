﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dal;
using Dto;

namespace Bussiness
{
    public class ApiBussiness
    {
        public ApiDal Dal;

        public ApiBussiness(string Conexion)
        {
            Dal = new ApiDal(Conexion);
        }



        public List<LoginDto> GetLogin(string Usuario, string Password)
        {
            return Dal.GetLogin(Usuario, Password);
        }
        public List<LoginDto> GetLoginToken(string Usuario, string Password)
        {
            return Dal.GetLoginToken(Usuario, Password);
        }

    }
}
